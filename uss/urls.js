      $(document).ready( function () {
       $('#data-table').dataTable( {
        "ordering": false,
          "ajax": "v/url.json",
          "deferRender": true,
          "columns": [
         { "data": "url" },
         { "data": "tarih" }
         ],
          "language": {
           "sInfo":           "Erişime engellenen URL sayısı: _TOTAL_",
           "infoEmpty":       "Erişime engellenen URL sayısı: ...",
           "sInfoFiltered":   "(arama filtresi ile eşleşen)",
           "sLengthMenu":     "Bir sayfada gösterilecek girdi sayısı: _MENU_",
           "sLoadingRecords": "Yükleniyor...",
           "sProcessing":     "İşleniyor...",
           "sSearch":         "Ara:",
           "sZeroRecords":    "Eşleşen kayıt bulunamadı.",
           "oPaginate": {
                "sNext":     ">",
                "sPrevious": "<"
           },
          },
          "dom": '<"top"iLf>t<"bottom"p><"clear">'
         });
       });

